const readline = require('readline')
const chalk = require('chalk')
const ScrumPokerLib = require('../src/index')

// Configure an await-able version of readline.question
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
const ask = q => new Promise(r => rl.question(q, r))

// Helper to show the results after a round
const showResults = cards => {
  console.log('')
  console.log(chalk.green.bold('### Results ###'))
  cards.forEach(c => {
    console.log(
      chalk.gray('-'),
      chalk.redBright(c.user),
      chalk.gray('voted'),
      chalk.redBright(c.value)
    )
  })
}

// Ask the user for a card, and verify it is a valid one
const pickCard = async (sp, first) => {
  const valid = sp.getValues()
  const askHand = (showValues) => {
    if (showValues) {
      console.log(chalk.gray('\nAllowed values: ' + valid.join(', ')))
    }
    return ask(chalk.cyan('Your hand: '))
  }

  let v = await askHand(first)
  while (valid.indexOf(v) === -1) {
    console.log(chalk.red('Invalid option.'))
    v = await askHand(true)
  }
  return v
}

// Main
const play = async () => {

  // Init library with the user name
  console.log('')
  const name = await ask(chalk.cyan('What\'s your name? '))
  const sp = new ScrumPokerLib(name)

  // Attach listeners
  sp.on('reveal', async cards => {
    showResults(cards)
    console.log('\n------------------------------\n')
    sp.pick(await pickCard(sp))
  })

  // Pick the first card
  sp.pick(await pickCard(sp, true))
}

// Start main
play()
