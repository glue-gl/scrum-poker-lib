Scrum Poker Lib
===============

**NOTE**: This is a mock library used for coding tests. Don't use it for real projects!

An easy to use library to coordinate Scrum Poker plays.


Usage
-----

Install it with:

```sh
npm install git+https://gitlab.com/glue-gl/scrum-poker-lib.git
```

Then you can use it like this:

```js
import ScrumPokerLib from 'scrum-poker-lib'

// Instance the library (usually only once) with your user name
const sp = new ScrumPokerLib('Alice')

// Attach event listeners for any events you want to handle
sp.on('reveal', cards => console.log(cards))

// Check which values can be picked for this hand
sp.getValues()

// Pick one of them, and wait for a reveal event
sp.pick(5)
```

For more info, check the demo on the demo folder.
