const names = ['Alice', 'Bob', 'Carol', 'Dave', 'Eve']
const values = ['1', '2', '3', '5', '8', '13', '20', '40', '100', '?']

exports = module.exports = class ScrumPokerLib {

  constructor(name) {
    this.name = name
    this.handlers = {}
  }

  on(event, handler) {
    this.handlers[event] = this.handlers[event] || []
    this.handlers[event].push(handler)
  }

  __trigger(event, ...args) {
    this.handlers[event].forEach(handler => {
      handler(...args)
    })
  }

  __othersPick(mine) {
    const peers = names.filter(n => n !== this.name)
    if (peers.length === 5) peers.pop()
    const votes = peers.map(n => ({
      user: n,
      value: values[Math.floor(Math.random() * values.length)]
    }))
    votes.push({ user: this.name, value: mine })
    this.__trigger('reveal', votes)
  }

  getValues() {
    return values
  }

  pick(value) {
    if (Math.random() > 0.7) {
      this.__othersPick(value)
    } else {
      setTimeout(() => this.__othersPick(value), 1000 + Math.random() * 4000)
    }
  }

}
